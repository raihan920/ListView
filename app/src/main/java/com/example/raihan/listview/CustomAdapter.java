package com.example.raihan.listview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by raihan on 23-Oct-17.
 */

public class CustomAdapter extends ArrayAdapter {
    String[] _myData;
    Context _context;
    public CustomAdapter(Context context, String[] myData) { //here CustomAdapter is a constructor
        super(context, 0, myData);
        _context = context;
        _myData = myData;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //find the position if even or odd
        if(position%2==0){
            convertView = inflater.inflate(R.layout.custom_list_layout, parent, false); //if even
        }else if(position%2!=0){
            convertView = inflater.inflate(R.layout.custom_list_layout_2, parent, false); //if even
        }

                                                                                    // create for odd   `
        TextView textIndex = (TextView) convertView.findViewById(R.id.txtIndex);
        TextView textName = (TextView) convertView.findViewById(R.id.txtName);

        String data = _myData[position];
        textIndex.setText(""+position);
        textName.setText(data);

        return convertView;
    }
}