package com.example.raihan.listview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    String msg = "Android : ";

    String[] mobileArray = {"Android","IPhone","WindowsMobile","Blackberry","WebOS","Ubuntu","Windows7", "Mac OS X","Android","IPhone","WindowsMobile","Blackberry","WebOS","Ubuntu","Windows7", "Mac OS X","Android","IPhone","WindowsMobile","Blackberry","WebOS","Ubuntu","Windows7", "Mac OS X"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /* -- androids default list view start -- */
//        ArrayAdapter myAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mobileArray);
//        ListView myListView1 = (ListView) findViewById(R.id.listView1);
//        myListView1.setAdapter(myAdapter1);
        /* -- androids default list view end -- */


        /* -- custom list view start -- */
        /*ArrayAdapter myAdapter1 = new ArrayAdapter<String>(this, R.layout.list_view_item, mobileArray);
        ListView myListView1 = (ListView) findViewById(R.id.listView1);
        myListView1.setAdapter(myAdapter1);*/
        /* -- custom list view end -- */

        /* -- list view with custom adapter start -- */
        CustomAdapter myAdapter2 = new CustomAdapter(MainActivity.this, mobileArray);
        ListView myListView2 = (ListView) findViewById(R.id.listView1);
        myListView2.setAdapter(myAdapter2);
        /* -- list view with custom adapter end -- */
    }

}